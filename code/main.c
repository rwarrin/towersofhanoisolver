/**
 * Iterative Algorithm
 * If N is odd, move tower A to C
 * if N is even, move tower A to B
 *
 * - No odd disc may be placed directly on another odd disc.
 * - No even disc my be placed directly on another even disc.
 * - There will sometimes be two possible Towers, one will have discs and the
 *   other will be empty. Place the disc in the non-empty Tower.
 * - Never move a disc twice in succession.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define Assert(Condition) if(!(Condition)) { *(int *)0 = 0; }

#define true 1
#define false 0
typedef uint32_t bool32;

static uint32_t GlobalMoveCounter = 0;

enum tower_index
{
	TowerIndex_A = 0,
	TowerIndex_B,
	TowerIndex_C,

	TowerIndex_Count
};

struct disc
{
	uint32_t Size;
	uint32_t TowerIndex;
	bool32 IsOdd;

	struct disc *NextDisc;
};

struct tower
{
	struct disc *Discs;
	uint32_t Id;
	uint32_t DiscCount;
	bool32 MoveAvailable;
};

static inline struct disc *
MoveDiscBetweenTowers(struct tower *Towers, uint32_t SourceTowerIndex, uint32_t TargetTowerIndex)
{
	++GlobalMoveCounter;
	struct tower *SourceTower = (Towers + SourceTowerIndex);
	struct tower *TargetTower = (Towers + TargetTowerIndex);
	struct disc *Disc = SourceTower->Discs;

	SourceTower->Discs = SourceTower->Discs->NextDisc;
	Disc->NextDisc = TargetTower->Discs;
	Disc->TowerIndex = TargetTowerIndex;
	TargetTower->Discs = Disc;

	--SourceTower->DiscCount;
	++TargetTower->DiscCount;

	printf("Move Disc %d from Tower %d to Tower %d\n", Disc->Size, SourceTowerIndex, TargetTowerIndex);
	return(Disc);
}

static inline bool32
DiscsAreOrderedOnTargetTower(struct tower *TargetTower, uint32_t NumberOfDiscs)
{
	bool32 Result = false;

	if(TargetTower->DiscCount == NumberOfDiscs)
	{
		for(struct disc *Disc = TargetTower->Discs;
			Disc->NextDisc != NULL;
			Disc = Disc->NextDisc)
		{
			if(Disc->Size >= Disc->NextDisc->Size)
			{
				break;
			}
		}

		Result = true;
	}

	return(Result);
}

int
main(int ArgCount, char **Args)
{
	uint32_t NumberOfDiscs = 3;
	if(ArgCount > 1)
	{
		if((Args[1][0] == '-') &&
		   (Args[1][1] == 'h'))
		{
			printf("Usage: %s <discs>\n", Args[0]);
			printf("Towers of Hanoi solver.\n");
			return 1;
		}
		else
		{
			NumberOfDiscs = atoi(Args[1]);
		}
	}

	uint32_t NumberOfTowers = 3;
	struct tower *Towers = (struct tower *)malloc(sizeof(struct tower) * NumberOfTowers);
	Assert(Towers != NULL);
	for(uint32_t TowerIndex = 0;
		TowerIndex < NumberOfTowers;
		++TowerIndex)
	{
		struct tower *Tower = (Towers + TowerIndex);
		Tower->Id = TowerIndex;
		Tower->Discs = NULL;
	}

	struct disc *Discs = (struct disc *)malloc(sizeof(struct disc) * NumberOfDiscs);
	Assert(Discs != NULL);

	uint32_t DiscSize = NumberOfDiscs;
	for(uint32_t DiscIndex = 0;
		DiscIndex < NumberOfDiscs;
		++DiscIndex)
	{
		struct disc *Disc = (Discs + DiscIndex);

		uint32_t TowerIndex = TowerIndex_A;
		Disc->Size = DiscSize--;
		Disc->IsOdd = (Disc->Size % 2 != 0);
		Disc->TowerIndex = TowerIndex;
		Disc->NextDisc = Towers[TowerIndex].Discs;
		Towers[TowerIndex].Discs = Disc;
	}

	struct disc *LastMovedDisc = NULL;
	if(NumberOfDiscs % 2 != 0)
	{
		LastMovedDisc = MoveDiscBetweenTowers(Towers, TowerIndex_A, TowerIndex_C);
	}
	else
	{
		LastMovedDisc = MoveDiscBetweenTowers(Towers, TowerIndex_A, TowerIndex_B);
	}

	while(!DiscsAreOrderedOnTargetTower(&Towers[NumberOfTowers - 1], NumberOfDiscs))
	{
		struct tower *SourceTower = NULL;
		struct disc *SourceDisc = NULL;
		for(uint32_t TowerIndex = 0;
			TowerIndex < NumberOfTowers;
			++TowerIndex)
		{
			struct tower *Tower = (Towers + TowerIndex);
			if((SourceDisc == NULL) &&
			   (Tower->Discs != NULL) &&
			   (Tower->Discs != LastMovedDisc))
			{
				SourceDisc = Tower->Discs;
				SourceTower = Tower;
			}

			if((Tower->Discs != NULL) &&
			   (SourceDisc != NULL) &&
			   (Tower->Discs->Size <= SourceDisc->Size) &&
			   (Tower->Discs->Size != LastMovedDisc->Size))
			{
				SourceDisc = Tower->Discs;
				SourceTower = Tower;
			}
		}

		Assert(SourceDisc != NULL);
		Assert(SourceTower != NULL);

		uint32_t MovesAvailable = 0;
		for(uint32_t TowerIndex = 0;
			TowerIndex < NumberOfTowers;
			++TowerIndex)
		{
			struct tower *Tower = Towers + TowerIndex;
			Tower->MoveAvailable = false;

			if((Tower->Discs != NULL) &&
			   ((Tower->Discs == SourceDisc) ||
				(Tower->Discs->IsOdd == SourceDisc->IsOdd) ||
				(Tower->Discs->Size <= SourceDisc->Size)))
			{
				continue;
			}

			Tower->MoveAvailable = true;
			++MovesAvailable;
		}

		for(uint32_t TowerIndex = 0;
			TowerIndex < NumberOfTowers;
			++TowerIndex)
		{
			struct tower *DestTower = (Towers + TowerIndex);
			if(DestTower->MoveAvailable)
			{
				if(MovesAvailable > 1)
				{
					if(DestTower->Discs != NULL)
					{
						LastMovedDisc = MoveDiscBetweenTowers(Towers, SourceTower->Id, DestTower->Id);
						break;
					}
				}
				else
				{
					LastMovedDisc = MoveDiscBetweenTowers(Towers, SourceTower->Id, DestTower->Id);
					break;
				}
			}
		}
	}
	printf("Solved in %d moves.\n", GlobalMoveCounter);

	free(Towers);
	free(Discs);
	return 0;
}
